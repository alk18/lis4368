> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368

## Allison Kulyk

### Assignment 1 Requirements:

Three Parts:

1. Distributed Version Control with Git and Bitbucket
2. Java/JSP/Servlet Development Installation
3. Chapter Questions (Chs 1-4)

#### README.md file should include the following items:

* Screenshot of running java.Hello (#1 above)
* Screenshot of running http://localhost9999 (#2 above, Step #4(b) in tutorial)
* git commands w/ short descriptions
* Bitbucket repo links a) this assignment and b) the completed tutorial above (bitbucketstationlocations)

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - creates a new git repository
2. git status - shows the status of the working directory and staging area
3. git add - moves pending changes from the working directory to the staging area
4. git commit - saves the currently staged changes
5. git push - uploads local repository content to a remote repository
6. git pull - downloads content from remote repository and updates the local repository to match it
7. git clone - downloads an existing repository to a local computer

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*: 

![AMPPS Installation Screenshot](img/tomcatInstall.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdkInstall.javaHello.png)

*A1 Screenshot*

![A1 Screenshot](img/localhost.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/alk18/bitbucketstationlocations/ "Bitbucket Station Locations")
