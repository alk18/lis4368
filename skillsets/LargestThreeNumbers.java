import java.util.Scanner;

public class LargestThreeNumbers
{
    public static void main (String[] args)
    {
        System.out.println("Developer: Allison Kulyk");

        System.out.println("Program evaluates largest of three integers.");

        System.out.println("Note: Program checks for integers and non-numeric values.");


        Scanner scnr = new Scanner(System.in);
        int num1 = 0; int num2 = 0; int num3 = 0;
        boolean isValidNum1 = false; boolean isValidNum2 = false; boolean isValidNum3 = false;
        
        System.out.printf("\nPlease enter first number: ");

        while(isValidNum1 == false) {

            if(scnr.hasNextInt())
            {
                isValidNum1 = true;
                num1 = scnr.nextInt();
            }
            else
            {
                System.out.println("Not valid integer!");
                System.out.printf("\nPlease try again. Enter first number: ");
            }
            scnr.nextLine(); //discard any other data entered on nextLine
        }

        System.out.printf("\nPlease enter second number: ");

        while(isValidNum2 == false) {

            if(scnr.hasNextInt())
            {
                isValidNum2 = true;
                num2 = scnr.nextInt();
            }
            else
            {
                System.out.println("Not valid integer!");
                System.out.printf("\nPlease try again. Enter second number: ");
            }
            scnr.nextLine(); //discard any other data entered on nextLine

        }

        System.out.printf("\nPlease enter third number: ");

        while(isValidNum3 == false) {

            if(scnr.hasNextInt())
            {
                isValidNum3 = true;
                num3 = scnr.nextInt();
            }
            else
            {
                System.out.println("Not valid integer!");
                System.out.printf("\nPlease try again. Enter third number: ");
            }
            scnr.nextLine(); //discard any other data entered on nextLine

        }

        if( num1 >= num2 && num1 >= num3)
        {
            System.out.println("First number is largest.");
        }

      else if (num2 >= num1 && num2 >= num3)
         {
            System.out.println("Second number is largest.");
         }

      else
         {
            System.out.println("Third number is largest.");
         }

    }
}