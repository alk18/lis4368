import java.io.*;
import java.util.Scanner;

public class FileWriteReadCountWords
{
    public static void main(String[] args) throws Exception
    {
        System.out.println("Developer: Allison Kulyk");
        System.out.println("Program captures user input, writes to and reads from file, and counts number of words in file.");
        System.out.println("Hint: use hasNext() method to read number of words (tokens).");
        System.out.println();

        String myFile = "filecountwords.txt";

        try {
            //create File object
            File file = new File(myFile);

            //create Printwriter object
            PrintWriter writer = new PrintWriter(file);

            //create Scanner object for user input
            Scanner input = new Scanner(System.in);

            //create String object to store user input
            String str = "";

            System.out.print("Please enter text: ");
            str = input.nextLine();

            //write to file using PrintWriter write() method
            writer.write(str);

            System.out.println("Saved to file \"" + myFile + "\"");

            //close output file == otherwise, open printWriter stream
            writer.close();

            //Scanner road = new Scanner(new FileInputStream(file));
            Scanner read = new Scanner(new FileInputStream(file));
            int count = 0;
            while(read.hasNext())
            {
                read.next();
                count++;
            }

            System.out.println("Number of words: " + count);
        }
        catch(IOException ex)
        {
            System.out.println("Error writing to file '" + myFile + "'");
        }
    }
}