import java.util.Scanner;

public class SimpleCalculatorVoidMethods
{
    public static void main (String[] args)
    {
        System.out.println("Developer: Allison Kulyk");

        System.out.println("Program uses nonvalue-returning methods to add, subtract, multiply, divide and power floating point numbers, rounded to two decimal places.");

        System.out.println("Note: Program checks for non-numeric values, and division by zero.");


        Scanner scnr = new Scanner(System.in);
        //String mathOp;
        float num1 = 0; float num2 = 1;
        boolean isValidNum1 = false; boolean isValidNum2 = false;
        

        System.out.printf("\nPlease enter mathematical operation (a=add, s=subtract, m=multiply, d=divide, p=power): ");
         char letter = scnr.next().charAt(0);
        
 
           // System.out.printf("\nIncorrect operation. Please enter correct operation: ");
           // scnr.nextLine(); //discard any other data entered on nextLine
            String mathOp = scnr.nextLine();
        
        System.out.printf("\nPlease enter first number: ");

        while(isValidNum1 == false) {

            if(scnr.hasNextFloat())
            {

                isValidNum1 = true;
                num1 = scnr.nextFloat();
            }
            else
            {
                System.out.println("Not valid number!");
                System.out.printf("\nPlease try again. Enter first number: ");
            }
            scnr.nextLine(); //discard any other data entered on nextLine
        }

        System.out.printf("\nPlease enter second number: ");

       while(isValidNum2 == false) {

           if(scnr.hasNextFloat())
            {
                isValidNum2 = true;
                
                
                if(mathOp.equals("d"))
                {
                    float num0 = scnr.nextFloat();
                    if(num0 == 0)
                    {
                        isValidNum2 = false;
                        System.out.println("Cannot divide by zero!");
                        break;
                                            }
                }
                num2 = scnr.nextFloat();
            }
            else
            {
                System.out.println("Not valid number!");
                System.out.printf("\nPlease try again. Enter second number: ");
            }
            scnr.nextLine(); //discard any other data entered on nextLine
        }

                 //if(mathOp.equals("p"))
                // {
                        System.out.println("\n" + num1 + " to the power of " + num2 + " = " + Math.pow(num1,num2));
 
               //  }

    }
}
