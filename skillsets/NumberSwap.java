import java.util.Scanner;

public class NumberSwap
{
    public static void main (String[] args)
    {
        System.out.println("Developer: Allison Kulyk");

        System.out.println("Program swaps two integers.");

        System.out.println("Note: Program checks for integers and non-numeric values.");

        
        Scanner scnr = new Scanner(System.in);
        int num1 = 0; int num2 = 0;
        boolean isValidNum1 = false; boolean isValidNum2 = false;
        
        System.out.printf("\nPlease enter first number: ");

        while(isValidNum1 == false) {

            if(scnr.hasNextInt())
            {
                isValidNum1 = true;
                num1 = scnr.nextInt();
            }
            else
            {
                System.out.println("Not valid integer!");
                System.out.printf("\nPlease try again. Enter first number: ");
            }
            scnr.nextLine(); //discard any other data entered on nextLine
        }

        System.out.printf("\nPlease enter second number: ");

        while(isValidNum2 == false) {

            if(scnr.hasNextInt())
            {
                isValidNum2 = true;
                num2 = scnr.nextInt();
            }
            else
            {
                System.out.println("Not valid integer!");
                System.out.printf("\nPlease try again. Enter second number: ");
            }
            scnr.nextLine(); //discard any other data entered on nextLine

        }

System.out.println("\nBefore Swapping\n" + "num1 = " + num1 +"\nnum2 = " + num2);

//swapping value of two numbers without using temp variable
num1 = num1+ num2; //now a is 30 and b is 20
num2 = num1 -num2; //now a is 30 but b is 10 (original value of a)
num1 = num1 -num2; //now a is 20 and b is 10, numbers are swapped

System.out.println("\nAfter Swapping\n" + "num1 = " + num1 +"\nnum2 = " + num2);

    }
}