import java.util.Scanner;

public class CountCharacter
{
    public static void main(String[] args)
    {
        //Display operational messages
        System.out.println("Developer: Allison Kulyk");
        System.out.println("Program counts number and types of characters: that is, letters, spaces, numbers, and other characters.");
        System.out.println("Hint: you may find the following methods helpful: isLetter(), isDigit(), isSpaceChar().");
        System.out.println("Additionally, you could add the functionality of testing for upper vs lower case letters.");
        System.out.println();

        //initialize variables
        int letter = 0;
        int space = 0;
        int num = 0;
        int other = 0;
        String str = "";

        System.out.printf("Please enter string: ");

        Scanner input = new Scanner(System.in);
        str = input.nextLine();

        char[] ch = str.toCharArray();

        for(int i = 0; i < str.length(); i++)
        {
            if(Character.isLetter(ch[i]))
            {
                letter++;
            }
        else if(Character.isDigit(ch[i]))
            {
                num++;
            }
        else if(Character.isSpaceChar(ch[i]))
        {
            space++;
        }
        else
        {
            other++;
        }
    }
    System.out.println("\nYour String: \"" + str + "\" has the following numbers and types of characters:");
    System.out.println("Letter(s): " + letter);
    System.out.println("Space(s): " + space);
    System.out.println("Number(s): " + num);
    System.out.println("Other character(s): " + other);
    
    }
}