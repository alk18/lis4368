> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 

## Allison Kulyk

### Assignment 4 Requirements:

Deliverables:

1. Provide BitBucket read-only access to LIS4368 repo, include links to the other assignment repos using Markdown syntax
2. Provide screenshots to failed and passed validation on the LIS4368 Local Web App
3. Provide screenshots of skillsets 10-12 
4. Chapter questions (Chs 11, 12)

#### README.md file should include the following items:

* Screenshot of Failed Validation
* Screenshot of Passed Validation
* Screenshot of skillsets 10-12
* Link to LIS4368 Local Web App


#### Project Screenshots:

*Screenshot of Failed Validation*:

![Failed Validation Screenshot](img/failedValidation.png)

*Screenshot of Passed Validation*:

![Passed Validation Screenshot](img/passedValidation.png)

*Screenshot of Skillset 10*:
![Skillset 10](img/skillset10.png)

*Screenshot of Skillset 11*:
![Skillset 11](img/skillset11.png)

*Screenshot of Skillset 12*:
![Skillset 12A](img/skillset12A.png)
![Skillset 12B](img/skillset12B.png)
![Skillset 12C](img/skillset12C.png)




*Links*:

[LIS4368 Local Web App](http://localhost:9999/lis4368/index.jsp "Local Web App")