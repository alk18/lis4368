> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 

## Allison Kulyk

### Assignment 5 Requirements:

Deliverables:

1. Provide BitBucket read-only access to LIS4368 repo, include links to the other assignment repos using Markdown syntax
2. Provide screenshots to user entry, passed validation, and associated database entry on the LIS4368 Local Web App
3. Provide screenshots of skillsets 13-15 
4. Chapter questions (Chs 13-15)


#### README.md file should include the following items:

* Screenshot of User Entry
* Screenshot of Passed Validation
* Screenshot of skillsets 13-15
* Link to LIS4368 Local Web App


#### Project Screenshots:

*Screenshot of Valid User Entry*:

![Valid User Entry Screenshot](img/userEntry.png)

*Screenshot of Passed Validation*:

![Passed Validation Screenshot](img/passedValidation.png)

*Screenshot of Associated Database Entry*:

![Associated Database Entry](img/databaseEntry.png)

*Screenshot of Skillset 13*:
![Skillset 10](img/skillset13.png)

*Screenshot of Skillset 14*:
![Skillset 11](img/skillset14.png)

*Screenshots of Skillset 15*:
![Skillset 12A](img/skillset15A.png)
![Skillset 12B](img/skillset15B.png)


*Links*:

[LIS4368 Local Web App](http://localhost:9999/lis4368/index.jsp "Local Web App")