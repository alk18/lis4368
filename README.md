> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS4368 - Advanced Web Applications

## Allison Kulyk

### LIS4368 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK
    - Install Tomcat
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorials
      (bitbucketstationlocations)
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Connect to mySQL through AMPPS
    - Provide screenshots of installations

3. [A3 README.md](a3/README.md "My A2 README.md file")
    - Create a data repository
    - Follow business rules
    - Save SQL code and ERD model
    - Forward-engineer to your local repository


4. [A4 README.md](a4/README.md "My A2 README.md file")
    - Screenshot of Failed Validation
    - Screenshot of Passed Validation
    - Screenshot of skillsets 10-12
    - Link to LIS4368 Local Web App

5. [A5 README.md](a5/README.md "My A2 README.md file")
    - Screenshot of Valid User Form Entry
    - Screenshot of Passed Validation
    - Screenshot of Associated Database Entry
    - Screenshot of skillsets 13-15
    - Link to LIS4368 Local Web App

6. [P1 README.md](p1/README.md "My A2 README.md file")
    - Screenshot of LIS4368 Portal
    - Screenshot of Failed Validation
    - Screenshot of Passed Validation
    - Link to LIS4368 Local Web App

7. [P2 README.md](p2/README.md "My A2 README.md file")
    - Screenshot of Valid User Form Entry
    - Screenshot of Passed Validation
    - Screenshot of Displayed Data (customers.jsp)
    - Screenshot of Modify Form (modify.jsp)
    - Screenshot of Modified Data (customers.jsp)
    - Screenshot of Delete Warning (customers.jsp)
    - Screenshot of Associated Database Changes (Select, Insert, Update, Delete)
