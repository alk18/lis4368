> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 

## Allison Kulyk

### Assignment 2 Requirements:

Three parts:

1. Connect to mySQL through AMPPS
2. Create servlets
3. Chapter Questions (Chs 5, 6)

#### README.md file should include the following items:

* Assessment links
* Only *one* screenshot of the query results from the following link: http://localhost:9999/hello/querybook.html

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

*Screenshot of http://localhost:9999/hello:*

![http://localhost:9999/hello Screenshot](img/localhostHello.png)

*Screenshot of http://localhost:9999/hello/HelloHome.html:*

![http://localhost:9999/hello/HelloHome.html Screenshot](img/helloHelloHome.png)

*Screenshot of http://localhost:9999/hello/sayhello:*

![http://localhost:9999/hello/sayhello Screenshot](img/helloSayHello.png)

*Screenshot of http://localhost:9999/hello/sayhi:*

![http://localhost:9999/hello/sayhi Screenshot](img/helloSayHi.png)

*Screenshot of http://localhost:9999/hello/querybook.html:*

![http://localhost:9999/hello/querybook.html Screenshot](img/helloQueryBook.png)

*Screenshot of http://localhost:9999/hello/query?author=Tan+Ah+Teck:*

![http://localhost:9999/hello/query?author=Tan+Ah+Teck Screenshot](img/helloQueryResult.png)

*Screenshot of local A2 website:*

![local a2 Screenshot](img/web.png)
