> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 

## Allison Kulyk

### Assignment 3 Requirements:

Deliverables:

1. Entity Relationship Diagram (ERD)
2. Include data (at least 10 records each table)
3. Chapter questions (Chs 7, 8)

#### README.md file should include the following items:

* Screenshot of ERD that links to the image
* Screenshot of a3 index.jsp
* Link to a3.sql and a3.mwb


#### Assignment Screenshots:

*Screenshot of A3 ERD*

![A3 ERD Screenshot](img/a3ERD.png)

*Screenshot of running Local Web App*:

![Local Web App Screenshot](img/a3index.png)

*Links*:

[A3 MWB File](docs/a3.mwb "A3 ERD in .mwb format")

[A3 SQL File](docs/a3.sql "A3 SQL Script")