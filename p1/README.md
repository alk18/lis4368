> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 

## Allison Kulyk

### Project 1 Requirements:

Deliverables:

1. Provide BitBucket read-only access to LIS4368 repo, include links to the other assignment repos using Markdown syntax
2. Provide screenshots to failed and passed validation on the LIS4368 Local Web App
3. Chapter questions (Chs 9, 10)

#### README.md file should include the following items:

* Screenshot of LIS4368 Portal
* Screenshot of Failed Validation
* Screenshot of Passed Validation
* Link to LIS4368 Local Web App


#### Project Screenshots:

*Screenshot of LIS4368 Portal*

![Portal Screenshot](img/portal.png)

*Screenshot of Failed Validation*:

![Failed Validation Screenshot](img/failedValidation.png)

*Screenshot of Passed Validation*:

![Passed Validation Screenshot](img/passedValidation.png)

*Links*:

[LIS4368 Local Web App](http://localhost:9999/lis4368/index.jsp "Local Web App")