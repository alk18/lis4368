> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 

## Allison Kulyk

### Project 2 Requirements:

Deliverables:

1. Provide BitBucket read-only access to LIS4368 repo, include links to the other assignment repos using Markdown syntax
2. Provide screenshots to user entry, passed validation, displayed data, modify form, modified data, delete warning, and associated database changes on the LIS4368 Local Web App
3. Chapter questions (Chs 16-17)


#### README.md file should include the following items:

* Screenshot of User Entry
* Screenshot of Passed Validation
* Displayed Data
* Modify Form
* Modified Data
* Delete Warning
* Associated Database Changes


#### Project Screenshots:

*Screenshot of Valid User Entry*:

![Valid User Entry Screenshot](img/userInput.png)

*Screenshot of Passed Validation*:

![Passed Validation Screenshot](img/passedValidation.png)

*Screenshot of Displayed Data*:

![Passed Validation Screenshot](img/displayData.png)

*Screenshot of Modify Form*:

![Modify Form Screenshot](img/mod.png)

*Screenshot of Modified Data*:

![Modified Data Screenshot](img/modData.png)

*Screenshot of Delete Warning*:

![Delete Warning Screenshot](img/del.png)

*Screenshot of Associated Database Changes*:

![Associated Database Changes 1](img/table1.png)
![Associated Database Changes 2](img/table2.png)
